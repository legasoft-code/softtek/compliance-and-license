package com.softtek.melia.pocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplianceAndLicenseApplication {
    public static void main(String[] args) {
        SpringApplication.run(ComplianceAndLicenseApplication.class, args);
    }
}